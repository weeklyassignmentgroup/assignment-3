﻿using System.ComponentModel.DataAnnotations;
namespace MovieCharactersAPI.Models;
public class Character
{
    [Key]
    public int Id { get; set; }
    [MaxLength(64)]
    public string FullName { get; set; }
    [MaxLength(64)]
    public string? Alias { get; set; }
    [MaxLength(32)]
    public string Gender { get; set; }
    [MaxLength(256)]
    public string? Picture { get; set; }

    // navigation properties
    public ICollection<Movie> Movies { get; set; }
}
