﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Interfaces
{
    public interface IMovieRepository : IRepository<Movie>
    {
        /// <summary>
        /// Return all characters in the movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of characters</returns>
        public Task<List<Character>> GetCharacters(int id);
        /// <summary>
        /// Sets all characters participating in a movie. 
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns>A list of characters added to the movie</returns>
        public Task<List<Character>> UpdateCharacters(int movieId, int[] characterIds);
    }
}
