﻿using MovieCharactersAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs
{
    public class CharacterReadDto
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string? Alias { get; set; }
        public string Gender { get; set; }

        public string? Picture { get; set; }

    }
}
