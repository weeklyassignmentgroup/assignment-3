﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Data
{
    public class MovieCharactersDbContext : DbContext
    {
        #nullable disable
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharactersDbContext(DbContextOptions<MovieCharactersDbContext> options) : base (options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Franchise>()
                .HasMany(f => f.Movies)
                .WithOne(m => m.Franchise)
                .OnDelete(DeleteBehavior.SetNull);

            List<Character> characters = SeedDataHelper.GetCharacters();
            List<Movie> movies = SeedDataHelper.GetMovies();
            List<Franchise> franchises = SeedDataHelper.GetFranchises();

            modelBuilder.Entity<Character>().HasData(characters);
            modelBuilder.Entity<Movie>().HasData(movies);
            modelBuilder.Entity<Franchise>().HasData(franchises);

            SeedDataHelper.SeedM2MTableCharacterMovie(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }


#nullable restore



    }
}
