﻿using AutoMapper;
using MovieCharactersAPI.DTOs;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<MovieCreateDto, Movie>();
            CreateMap<Movie, MovieReadDto>().ReverseMap();
            CreateMap<MovieUpdateDto, Movie>();
        }
    }
}
