﻿using AutoMapper;
using MovieCharactersAPI.DTOs;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<CharacterCreateDto, Character>();
            
            CreateMap<CharacterReadDto, Character>().ReverseMap();
            CreateMap<CharacterUpdateDto, Character>();
        }
    }
}
