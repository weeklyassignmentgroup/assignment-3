# Assignment 3 - Movie Characters API

## Contributors
- [Mussa Banjai](https://gitlab.com/MoBanju)
- [Vebjørn Sundal](https://gitlab.com/vebsun95)

## Description 
 A .net 6 WebApi for accessing a movie-franchise Database, using Entity framework and API controllers. ¯\\_(ツ)_/¯

## Dependencies
- .net 6
- Microsoft SQL
- AutoMapper
- AutoMapper.Extensions.Microsoft.DependencyInjection
- Microsoft.EntityFrameworkCore
- Microsoft.EntityFrameworkCore.SqlServer
- Microsoft.EntityFrameworkCore.Tools
- Microsoft.VisualStudio.Web.CodeGeneration.Design
- Swashbuckle.AspNetCore

## Movie Catalog DB
![Diagram_A](../Docs/Assignment-3.A-diagram.png)
